package server;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;

public class Server {
    public static void main(String[] args) throws IOException {
        ServerSocket ss = new ServerSocket(8080);
        System.out.println("server is running ... ");
        while (true) {
            Socket socket = ss.accept() ;
            System.out.println("connected from " + socket.getRemoteSocketAddress());
            Thread t =new Handler(socket);
            t.start();
        }
    }
}

class Handler extends Thread {
    Socket socket ;
    public Handler(Socket socket){
        this.socket = socket ;
    }

    @Override
    public  void run (){
        try(InputStream input = this.socket.getInputStream()){
            try(OutputStream output = this.socket.getOutputStream()){
                handle(input,output);
            }
        }catch (Exception e ) {
            try{
                this.socket.close();
            }catch (IOException ioe){
                ioe.printStackTrace();
            }
            System.out.println("client disconnected.");
        }
    }
    private void handle(InputStream input, OutputStream output ) throws IOException {
        var reader = new BufferedReader(new InputStreamReader(input, StandardCharsets.UTF_8));
        var writer = new BufferedWriter(new OutputStreamWriter(output, StandardCharsets.UTF_8));

        // 读取HTTP请求
        boolean requestOK = false;
        String first = reader.readLine() ;
        if(first.startsWith("GET / HTTP/1.")){
            requestOK = true ;
        }
        while(true){
            String header = reader .readLine() ;
            if(header.isEmpty()){
                break;
            }
            System.out.println(header);
        }
        System.out.println(requestOK ? "Response OK":"Response Error");
        if(!requestOK){
            // 响应错误
            writer.write("HTTP/1.0 404 Not Found\r\n");
            writer.write("Content-Length: 0\r\n");
            writer.write("\r\n");
            writer.flush();
        }else{
            // 响应成功
            String data = "<html><body><h1>Hello, world!</h1></body></html>";
            int length = data.getBytes(StandardCharsets.UTF_8).length;
            writer.write("HTTP/1.0 200 OK\r\n");
            writer.write("Connection: close\r\n");
            writer.write("Content-Type: text/html\r\n");
            writer.write("Content-Length: " + length + "\r\n");
            writer.write("\r\n"); // 空行标识Header和Body的分隔
            writer.write(data);
            writer.flush();
        }
    }
}