package udp;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.nio.charset.StandardCharsets;

public class Server {
    private static int LISTEN_PORT = 6666 ;
    public static void main(String[] args) throws IOException {
        /// 监听udp数据包
        DatagramSocket ds = new DatagramSocket(LISTEN_PORT);
        while ( true ) {
            byte [] buffer = new byte[1024] ;
            // DatagramPacket 接收udp数据包
            DatagramPacket packet = new DatagramPacket(buffer,buffer.length);
            ds.receive(packet);
            String s = new String (packet.getData(),packet.getOffset(),packet.getLength(), StandardCharsets.UTF_8);
            byte []data= "ACK".getBytes(StandardCharsets.UTF_8);
            packet.setData(data);
            ds.send(packet);
        }
    }
}
