package tcp;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;



public class Server {

    /// 服务端监听端口服务端口,socket收到从getRemoteSocketAddress的端口
    private static int LISTEN_PORT = 6666 ;

    public static void main(String[] args) throws IOException {
        ServerSocket serverSocket = new ServerSocket(LISTEN_PORT) ;
        System.out.println( "Server is running ... " ) ;
        while ( true ) {
            Socket socket = serverSocket.accept() ;
            System.out.println( "Connected from "+socket.getRemoteSocketAddress() );
            Thread thread = new Handler(socket);
            // 开始新线程
            thread.start();
        }
    }


}
class Handler extends Thread{
    Socket socket ;
    public Handler(Socket socket) {
        this.socket = socket ;
    }

    @Override
    public void run(){
        try(InputStream input = this.socket.getInputStream()){
            try(OutputStream output = this.socket.getOutputStream()){
                handle(input,output);
            }
        }catch (Exception e ){
            try {
                this.socket.close();
            }catch (IOException ioe){
            }
            System.out.println("client disconnected .");
        }
    }


    private void handle(InputStream input , OutputStream output) throws IOException{
        var writer = new BufferedWriter(new OutputStreamWriter(output, StandardCharsets.UTF_8));
        var reader = new BufferedReader(new InputStreamReader(input,StandardCharsets.UTF_8));
        writer.write("hello\n");
        writer.flush();
        while(true){
            String s = reader.readLine() ;
            System.out.println("Server receives \" "+s+"\" form port "+socket.getRemoteSocketAddress());
            if(s.equals("bye")){
                writer.write("bye\n");
                writer.flush();
                break;
            }
            writer.write("ok,"+s+"\n");
            writer.flush();
        }
    }
}