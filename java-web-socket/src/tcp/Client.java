package tcp;

import java.io.*;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

public class Client implements Runnable {

    private Socket socket ;
    public Client(String host,int port) throws IOException {
        /// 连接到的主机
        this.socket = new Socket(host,port);
    }
    private void handle(InputStream inputStream,OutputStream outputStream) throws IOException {
        var writer = new BufferedWriter(new OutputStreamWriter(outputStream, StandardCharsets.UTF_8));
        var reader = new BufferedReader(new InputStreamReader(inputStream,StandardCharsets.UTF_8));
        Scanner scanner = new Scanner(System.in);
        System.out.println("[server]"+reader.readLine());
        while( true ) {
            System.out.println(">>> ");
            String s = scanner.nextLine() ;
            writer.write(s);
            writer.newLine();
            writer.flush();
            String resp = reader.readLine() ;
            System.out.println("<<<"+resp);
            if(resp.equals("bye")){
                break;
            }
        }
    }

    @Override
    public void run() {
        try(InputStream inputStream=socket.getInputStream()){
            try(OutputStream outputStream=socket.getOutputStream()){
                try {
                    handle(inputStream,outputStream);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }catch (IOException e){
            e.printStackTrace();
        }
        try {
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("disconnected.") ;
    }
}
