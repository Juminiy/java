package com.hua.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.annotation.WebListener;

@WebListener
public class AppListener {
    public void contextInitialized(ServletContextEvent servletContextEvent){
        // 初始化webapp 例如打开数据库连接池
        System.out.println("WebApp initialized.");
    }

    // 清理WebApp ，关闭sql-connection等服务
    public void contextDestroyed(ServletContextEvent servletContextEvent){
        System.out.println("WebApp destroyed.");
    }
}
