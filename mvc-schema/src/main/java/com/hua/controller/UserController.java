package com.hua.controller;

import com.hua.bean.SignInBean;
import com.hua.bean.User;
import com.hua.framework.GetMapping;
import com.hua.framework.ModelAndView;
import com.hua.framework.PostMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserController {
	//模拟数据库
	private Map<String, User> userDatabase = new HashMap<>() {
		{
			List<User> users = List.of( //
					new User("bob@example.com", "bob123", "Bob", "This is bob."),
					new User("tom@example.com", "tomcat", "Tom", "This is tom."));
			users.forEach(user -> {
				put(user.email, user);
			});
		}
	};

	@GetMapping("/signin")
	public ModelAndView signin(HttpServletRequest httpServletRequest) {
		// 登陆了直接进入 "/"
		if(httpServletRequest.getSession().getAttribute("user")!=null){
			System.out.println("Login in already redirect to \"/\" ");
			return new ModelAndView("redirect:/");
		}else{
			return new ModelAndView("/signin.html");
		}
	}

	@PostMapping("/signin")
	public ModelAndView doSignin(SignInBean bean, HttpServletResponse response, HttpSession session)
			throws IOException {
		User user = userDatabase.get(bean.email);
		if (user == null || !user.password.equals(bean.password)) {
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			PrintWriter pw = response.getWriter();
			if( user == null ){
				pw.write("{\"error\":\"用户不存在\"}");
			} else{
				pw.write("{\"error\":\"密码错误\"}");
			}
			pw.flush();
		} else {
			session.setAttribute("user", user);
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			PrintWriter pw = response.getWriter();
			pw.write("{\"result\":true}");
			pw.flush();
		}
		return null;
	}

	@GetMapping("/signout")
	public ModelAndView signout(HttpSession session) {
		session.removeAttribute("user");
		return new ModelAndView("redirect:/");
	}

	@GetMapping("/user/profile")
	public ModelAndView profile(HttpSession session) {
		User user = (User) session.getAttribute("user");
		if (user == null) {
			return new ModelAndView("redirect:/signin");
		}
		return new ModelAndView("/profile.html", "user", user);
	}
}
