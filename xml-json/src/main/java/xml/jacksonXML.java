package xml;

import com.fasterxml.jackson.dataformat.xml.JacksonXmlModule;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import pojo.Book;

import java.io.File;
import java.io.IOException;

public class jacksonXML {
    public static void main(String[] args) throws IOException {
        File file =new File("D:\\Code\\IDEAProject\\java\\xml-json\\src\\main\\resources\\book.xml");
        JacksonXmlModule module = new JacksonXmlModule();
        XmlMapper mapper = new XmlMapper(module);
        Book book = mapper.readValue(file, Book.class);
        System.out.println(book.id);
        System.out.println(book.name);
        System.out.println(book.author);
        System.out.println(book.isbn);
        System.out.println(book.tags);
        System.out.println(book.pubDate);
    }
}
