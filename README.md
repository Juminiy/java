# java 网络基础学习
 **为了方便导jar包依赖，除了java-web-socket外的所有子模块均为maven项目**

## java网络编程
子模块名：java-basic-network 和 java-web-socket

包含：

1. 简单的计算机网络知识
2. java-socket tcp/udp通信
3. smtp/pop3 收发邮箱
4. http/https 发送请求包
5. get/post/delete http方法

## java-web基础

包含：
1. http 请求响应过程
2. mvc三层架构
3. servlet开发

### servlet基础
子模块名：http-server
### 模仿springboot写一个内嵌tomcat主启动类
子模块名：web-servlet-embedded
### xml与json格式文件
子模块名：xml-json

## mvc基础
### 手写一个简易版mvc框架：借鉴了spring-mvc
子模块名 mvc-schema

## 设计模式 
 design-pattern
敬请期待 ... 

**每个子模块里面的target目录如果不需要可以删除**


> 与这个代码配套学习的笔记 [地址](https://blogs.hulingnan.site/archives/java-web)
> 也欢迎到我的[个人博客](https://blogs.hulingnan.site)